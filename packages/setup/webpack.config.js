const path = require('path');
module.exports = {
  entry: './src/setup.ts',
  target: 'node',
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        loader: 'ts-loader',
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
      },
    ],
  },
  plugins: [],
  externals: [
    {
      // Possible drivers for knex - we'll ignore them
      // comment the one YOU WANT to use
      sqlite3: 'sqlite3',
      // mysql2: 'mysql2', // << using this one
      mariasql: 'mariasql',
      mysql: 'mysql',
      mssql: 'mssql',
      oracle: 'oracle',
      'strong-oracle': 'strong-oracle',
      oracledb: 'oracledb',
      pg: 'pg',
      tedious: 'tedious',
      'pg-query-stream': 'pg-query-stream',
      cardinal: 'cardinal',
    },
  ],
};
