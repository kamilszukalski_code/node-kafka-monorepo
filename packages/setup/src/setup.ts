import {
  createKafkaClient,
  kafkaCosumerConfig,
  knexMariaDbClient,
} from '@pokemon/common';
import fs from 'fs';
import path from 'path';

const setUpKafka = async () => {
  console.log('=== Setting up/Verifying Kafka ===');

  const kafka = createKafkaClient();
  const admin = kafka.admin();
  await admin.connect();

  const existingTopics = await admin.listTopics();
  if (existingTopics.includes(kafkaCosumerConfig.topic)) {
    console.log(
      `Topic: ${kafkaCosumerConfig.topic} is already created... nothing more to do here`
    );
  } else {
    console.log(
      `Topic: ${kafkaCosumerConfig.topic} deos NOT exist - let's create one...`
    );
    const result = await admin.createTopics({
      topics: [
        {
          topic: kafkaCosumerConfig.topic,
          numPartitions: 2,
          replicationFactor: 1,
        },
      ],
    });
    console.log(`Is topic: ${kafkaCosumerConfig.topic} created: ${result}`);
  }

  await admin.disconnect();

  console.log('--- Finished Setting up/Verifying Kafka ---\n\r\n\r');
};

const sleep = (ms: number) => {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
};

const setUpMariaDB = async () => {
  console.log('=== Setting up/Verifying Maria DB ===');
  const maxRetries = 10;
  let i = 0;
  let success = false;

  const rawSql = fs
    .readFileSync(path.join(__dirname, '/../../../docker/mariadb/setup.sql'))
    .toString();

  while (i < maxRetries) {
    try {
      await sleep(1000 * i);
      const res = await knexMariaDbClient.raw(`${rawSql}`);
      console.log(res);
      success = true;
      break;
    } catch (e) {
      console.log(
        `Attempt: ${i} Something went wrong during connection with db...`,
        e
      );
      i++;
    }
  }

  if (success) {
    console.log('--- Finished Setting up/Verifying Maria DB ---');
  } else {
    console.log('--- ERROR: DB could NOT be set up ---');
  }
};

const run = async () => {
  try {
    await setUpKafka();
  } catch (e) {
    console.log('Sth went wrong - could not setup kafka', e);
  }

  await setUpMariaDB();

  const exit = process.exit;
  exit(0);
};

run().catch(console.error);
