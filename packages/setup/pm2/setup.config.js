const path = require('path');
module.exports = {
  apps: [
    {
      name: 'setup-prod',
      script: './build/bundle.js',
      autorestart: false,
      instances: 1,
      combine_logs: false,
      out_file: path.join(__dirname + '/../logs/setup.log'),
      error_file: path.join(__dirname + '/../logs/setup.errors.log'),
      log_date_format: 'YYYY-MM-DD HH:mm:ss sss Z',
      exec_mode: 'fork',
      env: {
        NODE_ENV: 'production',
      },
    },

    {
      name: 'setup-local',
      script: './build/bundle.js',
      autorestart: false,
      instances: 1,
      combine_logs: false,
      out_file: path.join(__dirname + '/../logs/setup.log'),
      error_file: path.join(__dirname + '/../logs/setup.errors.log'),
      node_args: '-r dotenv/config',
      args: ['dotenv_config_path=./../../.env.local'],
      exec_mode: 'fork',
      log_date_format: 'YYYY-MM-DD HH:mm:ss sss Z',
      env: {
        NODE_ENV: 'development',
      },
    },
  ],
};
