const path = require('path');

module.exports = {
  ...require(path.join(
    __dirname + '../../../node_modules/gts/.prettierrc.json'
  )),
};
