const path = require('path');
module.exports = {
  apps: [
    {
      name: 'consumer-prod',
      script: './build/bundle.js',
      autorestart: false,
      instances: process.env.INSTANCES || 2,
      combine_logs: false,
      out_file: path.join(__dirname + '/../logs/consumer.log'),
      error_file: path.join(__dirname + '/../logs/consumer.errors.log'),
      exec_mode: 'cluster',
      env: {
        NODE_ENV: 'production',
      },
    },

    {
      name: 'consumer-local',
      script: './build/bundle.js',
      autorestart: false,
      instances: 1,
      combine_logs: false,
      out_file: path.join(__dirname + '/../logs/consumer.log'),
      error_file: path.join(__dirname + '/../logs/consumer.errors.log'),
      node_args: '-r dotenv/config',
      args: ['dotenv_config_path=./../../.env.local'],
      exec_mode: 'fork',
      env: {
        NODE_ENV: 'development',
      },
    },
  ],
};
