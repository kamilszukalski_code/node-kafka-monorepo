import {PokemonEncounter, PokemonEncounterService} from '@pokemon/common';
import {chunk} from 'lodash';

const pokemonEncounterService = new PokemonEncounterService();
const batchMariaDbSize = 5000;

export const processPokemonEncounters = async (
  newEncounters: PokemonEncounter[],
  heartbeat: () => Promise<void>
) => {
  if (newEncounters && newEncounters.length > 0) {
    let chunkIndex = 1;
    const insertChunks = chunk(newEncounters, batchMariaDbSize);
    for (const insertChunk of insertChunks) {
      console.log(
        `Performing INSERT chunk ${chunkIndex}/${insertChunks.length} on DB...`
      );
      await pokemonEncounterService.savePokemonEncounters(insertChunk);
      console.log(
        `Completed INSERT chunk ${chunkIndex}/${insertChunks.length} on DB!`
      );
      await heartbeat();
      chunkIndex++;
    }
  }
};
