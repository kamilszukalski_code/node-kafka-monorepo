import {EachBatchPayload} from 'kafkajs';
import * as moment from 'moment';
import {
  createKafkaClient,
  DateUtils,
  kafkaCosumerConfig,
} from '@pokemon/common';
import {processPokemonEncounters} from './processing/batch-processing.service';

const run = async () => {
  const kafka = createKafkaClient();

  const consumer = kafka.consumer({
    groupId: kafkaCosumerConfig.groupId,
    // sessionTimeout: 1000 * 60 * 10,
    // heartbeatInterval: 1000 * 60 * 5,
    // maxBytes: 5242880   // 2mb
  });

  await consumer.connect();
  await consumer.subscribe({
    topic: kafkaCosumerConfig.topic,
    fromBeginning: true,
  });
  await consumer.run({
    eachBatchAutoResolve: true,
    autoCommit: true,
    eachBatch: async ({batch, heartbeat}: EachBatchPayload) => {
      console.log(
        `Batch, messages=${batch.messages.length}, topic=${batch.topic}`
      );
      const startTime = moment();
      console.log(
        `[${startTime.format(
          DateUtils.DB_DATE_TEXT_FORMAT
        )}] Start batch processing.`
      );

      await processPokemonEncounters(
        batch.messages
          .filter(m => m.value !== null)
          .map(m => JSON.parse(m.value!.toString())),
        heartbeat
      );
      console.log(
        `[${moment().format(
          DateUtils.DB_DATE_TEXT_FORMAT
        )}] Finished batch processing. It took: ${moment().diff(
          startTime,
          'seconds'
        )} seconds\n\r`
      );
    },
  });
};

run().catch(console.log);
