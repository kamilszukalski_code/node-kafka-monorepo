export interface PokemonGetApiResponse {
  results: {
    name: string;
    url: string;
  }[];
}
