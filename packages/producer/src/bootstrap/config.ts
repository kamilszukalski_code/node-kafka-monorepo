const config = {
  pokemonUrl: process.env.FETCH_URL || 'please_setup_env:FETCH_URL',
  pokemonUsername:
    process.env.FETCH_USERNAME || 'please_setup_env:FETCH_USERNAME',
  pokemonPassword:
    process.env.FETCH_PASSWORD || 'please_setup_env:FETCH_PASSWORD',
  pokemonRequestTimeout: Number(process.env.FETCH_TIMEOUT) || Number.NaN,
};

export default config;
