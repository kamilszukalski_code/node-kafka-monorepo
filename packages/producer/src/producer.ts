import axios, {AxiosResponse} from 'axios';
import {CompressionTypes, ProducerRecord} from 'kafkajs';
import config from './bootstrap/config';
import moment = require('moment');

import axiosRetry from 'axios-retry';

import {PokemonGetApiResponse} from './dto/pokemon-api.response';
import {
  createKafkaClient,
  DateUtils,
  getRandomInt,
  kafkaCosumerConfig,
  PokemonEncounter,
} from '@pokemon/common';

axiosRetry(axios, {
  retries: 3,
  retryDelay: (retryCount: number) => {
    return retryCount * 1000;
  },
  shouldResetTimeout: true,
  retryCondition: error => {
    console.log(error.response?.status, 400, error.response?.status === 400);
    return [400, 429, 500, 502, 503, 504].includes(
      error.response?.status || 200
    );
  },
});

const kafka = createKafkaClient();
const producer = kafka.producer({idempotent: true});

const downloadData = async (numberOfPokemons: number) => {
  const startTime = moment.utc();
  const headers = {'Content-Type': 'application/json'};

  console.log('Sending request for all Pokemons in the world...');
  try {
    const response = await axios.get<
      never,
      AxiosResponse<PokemonGetApiResponse>
    >(config.pokemonUrl + `?limit=${numberOfPokemons}`, {
      headers: headers,
      timeout: config.pokemonRequestTimeout,
      auth: {
        username: config.pokemonUsername,
        password: config.pokemonPassword,
      },
    });
    console.log(
      `Received response from Professor Oak -> took : ${moment
        .utc()
        .diff(startTime, 'milliseconds')}ms, collected ${
        response.data.results.length
      }`
    );
    return response.data.results;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      console.log('Error', error.message);
    }
    return [];
  }
};

const run = async () => {
  console.log('=== Starting Pokemon Encounters Producer ===');

  const maxNumberOfPokemonTypes = 1000;

  const pokemons = await downloadData(maxNumberOfPokemonTypes);

  console.log('=== Waiting for encounters... ===');

  const minAmountOfMsgsPerBatch = 5000;
  const maxAmountOfMsgsPerBatch = 100000;

  const msgsNumber = getRandomInt(
    minAmountOfMsgsPerBatch,
    maxAmountOfMsgsPerBatch
  );
  const msgs: PokemonEncounter[] = [];
  const date: string = moment().format(DateUtils.DB_DATE_TEXT_FORMAT);

  for (let i = 0; i < msgsNumber; i++) {
    const pokemonOfIdToGet = getRandomInt(1, 1000);
    msgs.push({
      date: date,
      id: pokemonOfIdToGet,
      name: pokemons[pokemonOfIdToGet - 1].name,
    });
  }

  console.log(`I have prepared ${msgs.length}`);

  await producer.connect();

  const pokemonEncounters: ProducerRecord = {
    topic: kafkaCosumerConfig.topic,
    compression: CompressionTypes.GZIP,
    messages: [...msgs.map(p => ({value: JSON.stringify(p)}))],
  };

  await producer.send(pokemonEncounters);

  console.log('--- Finished Publishing Pokemon encounters to queue ---');

  const exit = process.exit;
  exit(0);
};

run().catch(console.error);
