export interface SentInformation {
  id: number;
  to: string;
  date: string;
  msg: string;
}
