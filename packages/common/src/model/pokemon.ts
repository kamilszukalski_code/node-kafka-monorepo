export interface Pokemon {
  id: number;
  name: string;
}

export interface PokemonEncounter extends Pokemon {
  date: string;
}
