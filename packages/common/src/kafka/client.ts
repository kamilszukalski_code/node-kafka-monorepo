import {Kafka} from 'kafkajs';

/**
 * Kafka connection parameters. Filled with values from .env
 */
const kafkaConfig = {
  /** The hostname at which Kafka is acessible. Should be combined with */
  hostname: process.env.KAFKA_HOSTNAME || 'please_setup_env:KAFKA_HOSTNAME',
  port: Number(process.env.KAFKA_PORT),
};

/**
 * Kafka connection parameters. Filled with values from .env
 */
const kafkaCosumerConfig = {
  groupId:
    process.env.KAFKA_CONSUMER_GROUPID_1 ||
    'please_setup_env:KAFKA_CONSUMER_GROUPID_1',
  groupId2:
    process.env.KAFKA_CONSUMER_GROUPID_2 ||
    'please_setup_env:KAFKA_CONSUMER_GROUPID_2',
  topic:
    process.env.KAFKA_CONSUMER_TOPIC || 'please_setup_env:KAFKA_CONSUMER_TOPIC',
};

const createKafkaClient = () => {
  return new Kafka({
    brokers: [`${kafkaConfig.hostname}:${kafkaConfig.port}`],
    retry: {
      retries: 10,
    },
  });
};

export {kafkaConfig, kafkaCosumerConfig, createKafkaClient};
