import knex from 'knex';

export const knexMariaDbClient = knex({
  client: 'mysql2',
  connection: {
    host: process.env.DB_HOST || 'please_setup_env:DB_HOST',
    port: Number(process.env.DB_PORT) || Number.NaN,
    database: process.env.DB_NAME || 'please_setup_env:DB_NAME',
    user: process.env.DB_USER || 'please_setup_env:DB_USER',
    password: process.env.DB_PASS || 'please_setup_env:DB_PASS',
    dateStrings: true,
    multipleStatements: true,
  },
  pool: {min: 2, max: 10},
});
