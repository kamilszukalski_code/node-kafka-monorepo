import {knexMariaDbClient} from '../db/knex';
import {PokemonEncounter} from '../model/pokemon';

export class PokemonEncounterService {
  #tableName = 'PokemonEncounters';

  async savePokemonEncounters(encounters: PokemonEncounter[]) {
    const res = await knexMariaDbClient(this.#tableName).insert(encounters);
    return res;
  }
}
