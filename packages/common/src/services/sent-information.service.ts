import {knexMariaDbClient} from '../db/knex';
import {SentInformation} from '../model/information';

const tableName = 'SentInformations';

const saveInfomrations = async (encounters: SentInformation[]) => {
  const res = await knexMariaDbClient(tableName).insert(encounters);
  return res;
};

export default {saveInfomrations};
