CREATE TABLE IF NOT EXISTS `PokemonEncounters` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  
  KEY `pokemon_id` (`id`),
  KEY `pokemon_name` (`name`)
);


CREATE TABLE IF NOT EXISTS `SentInformations` (
  `id` int(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `msg` TEXT NOT NULL,
  PRIMARY KEY (`id`)
);