# Pokemon Encounters App

This is a sample app showing how to set up project which requires multiple apps talking to each other in microservice architecture. Entire project is created as a monorepo with usage of library named `lerna`. It contains following packages:

- common - its responsibility is to store common/core pieces, which will be used by other packages (DRY)
- setup - a small app which will set up all necessary settings in kafka queue as well as will prepare db
- producer - app requesting data from REST api and then sending a batch of messages to the queue
- consumer - app connected to the queue and waiting for newly shown pokemons. Once it gets a new batch - it stores its data in db. Because there can be a heavy load of pokemons who are encountering us - it can be run in multiple instances with pm2 (docker-compose env controls it)

Technologies used:

- eslint + prettier - styling, preserving format/rules among all developers
- husky - don't allow to commit changes when some rules are not fullfilled
- lerna - setup and handling dependencies in monorepo
- kafka - widely used msg queue
- mariadb - RDBMS for storing data which has to be persisted
- pm2 - running multiple instances in docker containers (according to the number of threads in computer)
- knex - query builder
- kafkajs - kafka client for operating with data in kafka

In project tehre is a mix of classes/export const/indexes/static methods in order to show possibilities of current TS.

## Setup

From root:

```sh
# clears project - just in case if there would be trashes from previous interations
1. npx lerna clean
# bootstraps all packages of monorepo - uses hoist to create one shared node_modules for common node packages
2. npx lerna bootstrap --hoist
# prepares hooks for husky
3. npm run prepare
# builds common pakcage - necessary when we wants to develop in this project/make some changes
4. npm run build --workspace=@pokemon/common
# builds all custom images of our apps
5. docker-compose build
# starts core infrastructure - container of name setup is responsible for setting up kafka and db. After it finishes - it will stop itself to free resources
6. docker-compose up kafka zookeeper kafka-ui db setup

```

## Start in "View Mode"

In this mode you will run addtional packages also as containers from custom docker images. You will be able to see entire traffic but won't be able to introduce your chagnes to the code and see live reload;

```sh
# starts producer and consumers in parallel - after producer gets data from REST api - it will push it to the queue
# consumer will eventually get this data and operate with it further
7. docker-compose up producer consumer
```

To see msgs in the queue visit Kafka-UI dashbaord  
http://localhost:8080

To see persisted data in db - connect with your MySQL Workbench to localhost db with credentials and settings from .env.local
http://localhost:3306

## Start in "Development Mode"

In this mode you runs producer and consumer from your local machine instead of docker container - thanks to this you can perform changes to the code and have hot reload

```sh
# starts producer and consumers in parallel - after producer gets data from REST api - it will push it to the queue
# consumer will eventually get this data and operate with it further
7. npm run start --workspace=@pokemon/consumer
8. npm run start --workspace=@pokemon/producer
```

Or you can locally run those packages with pm2 - but you will loose hot reload option and will have to run any of following commands everytime you make changes in any of those packages

```sh
# starts producer and consumers in parallel - after producer gets data from REST api - it will push it to the queue
# consumer will eventually get this data and operate with it further
7. npm run start:pm2 --workspace=@pokemon/consumer
8. npm run start:pm2 --workspace=@pokemon/producer
9. npx pm2 monit
```

To see msgs in the queue visit Kafka-UI dashbaord  
http://localhost:8080

To see persisted data in db - connect with your MySQL Workbench to localhost db with credentials and settings from .env.local
http://localhost:3306

## Stop

To stop everything and clear entire infrastructre

```sh
1. stop:pm2
2. docker-compose down
3. ./scripts/docker/clear-entire-infrastructure.sh
```

## Helpful commands

### Lerna

```sh
#Bootstraps project and installs all dependencies in separate node_modules for each package. Automaticly creates symlinks for all necessary libs
lerna bootstrap

#Boorstraps project with shared node_modules folder in the root of project. Automaticly creates symlinks for all necessary libs
lerna boostrap --hoist

#Cleans all node_modules in project and symlinks
lerna clean

lerna add xxx --scope=yyyy
```

### VSCode

To restart vscode typescript server (usefull when vscode does not find changes in common module in monorepo)

```
1. Press CTRL + SHIFT + P
2. Type "typescript"
3. Select TypeScript: Restart TS server
```
