docker rm pokemon-kafka-producer --force --volumes
docker rm pokemon-kafka-consumer --force --volumes

docker rm pokemon-db --force --volumes

docker rm pokemon-kafka-maria-setup --force --volumes
docker rm pokemon-kafka-ui --force --volumes
docker rm pokemon-kafka --force --volumes
docker rm pokemon-zookeeper --force --volumes


 docker network rm pokemon-docker-network
 docker volume rm pokemon-kafka-volume
 docker volume rm pokemon-zookeeper-volume
 docker volume rm pokemon-kafka-maria-db-volume